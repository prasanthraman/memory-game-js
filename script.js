
const gameContainer = document.getElementById("game")

const gifs = [
  "./gifs/1.gif",
  "./gifs/2.gif",
  "./gifs/3.gif",
  "./gifs/4.gif",
  "./gifs/5.gif",
  "./gifs/6.gif",
  "./gifs/7.gif",
  "./gifs/8.gif",
  "./gifs/9.gif",
  "./gifs/10.gif",
  "./gifs/11.gif",
  "./gifs/12.gif"
]

const gifsWithDupes=[...gifs,...gifs]


// here is a helper function to shuffle an array
// it returns the same array with values shuffled
// it is based on an algorithm called Fisher Yates if you want ot research more
function shuffle(array) {
  let counter = array.length

  // While there are elements in the array
  while (counter > 0) {
    // Pick a random index
    let index = Math.floor(Math.random() * counter)

    // Decrease counter by 1
    counter--

    // And swap the last element with it
    let temp = array[counter]
    array[counter] = array[index]
    array[index] = temp
  }

  return array
}

let shuffledColors = shuffle(gifsWithDupes)

// this function loops over the array of colors
// it creates a new div and gives it a class with the value of the color
// it also adds an event listener for a click for each card
function createDivsForColors(colorArray) {
  let uniqueId = 0
  for (let color of colorArray) {
    // create a new div
    const newDiv = document.createElement("div")

    // give it a class attribute for the value we are looping over
    newDiv.classList.add(color)
    newDiv.classList.add(uniqueId)
    uniqueId++

    // call a function handleCardClick when a div is clicked on
    newDiv.addEventListener("click", handleCardClick)

    // append the div to the element with an id of game
    gameContainer.append(newDiv)
  }
}
document.querySelector('.completion button').addEventListener('click',()=>{
  location.reload()
})

// TODO: Implement this function!
let remains = 2
let targets = []
let matchedColor = []

function handleCardClick(event) {
  // you can use event.target to see which element was clicked
  if (event.target.classList.contains('on') || remains == 0 || matchedColor.includes(event.target.classList[0] || targets[0].classList.contains(event.target.classList[1]))) {
    //wait
    console.log("you clicked", event.target)
    if (event.target.classList.contains('matched')) {
      event.target.style.backgroundColor = event.target.classList[0]
      event.target.classList.add('on')

    }

  } else {

    console.log("you clicked", event.target)
    event.target.style.backgroundImage = `url("${event.target.classList[0]}")`
    event.target.classList.add('on')
    //console.log(event.target.classList[0],event.target.classList.length)
    //console.log(event.target.classList[1])
    remains--

    targets.push(event.target)
    if (targets.length == 2) {
      let colorA = targets[0].classList[0]
      let colorB = targets[1].classList[0]
      let identifierA = targets[0].classList[1]
      let identifierB = targets[1].classList[1]
      console.log(colorA, colorB)
      if (colorA == colorB && (identifierA !== identifierB)) {

        document.getElementById('score').textContent = (matchedColor.length + 1).toString()
        if(matchedColor.length+1 >=gifs.length){
          document.querySelector('.completion').style.display='flex'
        }
        targets.map((target) => {
          target.classList.add('matched')
          target.classList.add('on')
          target.style.backgroundImage = `url("${colorA}")`
          return target
        })

        matchedColor.push(colorA)
        targets=[]
      }else{
        targets.shift()
      }
    }

    setTimeout(() => {
      if (event.target.classList.contains('matched')) {
        event.target.style.backgroundColor = event.target.classList[0]
       

      } else {
        event.target.style.backgroundColor = 'lightpink'
        event.target.style.backgroundImage = 'none'
        event.target.classList.remove('on')
        console.log(targets)
    
        
      }
      console.log(event.target.classList)
      remains++
    }, 1.5 * 1000)
  }


}
document.addEventListener('DOMContentLoaded', () => {
  // when the DOM loads
  console.log(gifsWithDupes)
  createDivsForColors(shuffledColors)
})
